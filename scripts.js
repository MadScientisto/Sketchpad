$(document).ready(function(){

//Initial grid creation//
  for(i=1;i<=16*16;i++) {
    $('#main').append("<div class='pixels'></div>")
  }
  $(".pixels").height("60");
  $(".pixels").width("60");

  $(".pixels").hover(
    function(){
      $(this).css("background-color", "black")
    })
//Button grid creation//
  $('#resbutton').click(function(){
    if(!document.getElementById('resinput').value){
      alert('Give a resolution value please!')
    }
    else{
      $('#main').empty();
      $(".pixels").css("background-color", "white")
      var pixcount = document.getElementById('resinput').value;
      var pixdim = 960/pixcount;
      for(i=1;i<=pixcount*pixcount;i++) {
        $('#main').append("<div class='pixels'></div>")
      }
      $(".pixels").height(pixdim);
      $(".pixels").width(pixdim);

      $(".pixels").hover(
        function(){
          $(this).css("background-color", "black")
        })
    }
  })

})
